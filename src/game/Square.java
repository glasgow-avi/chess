package game;

import pieces.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

public class Square extends JButton
{
    private int row, col;
    private static final Color selected = new Color(255, 255, 0000, 180);
    public Piece currentPiece;

    private static Font chessFont;

    static
    {
        try
        {
            Font.createFont(Font.TRUETYPE_FONT, new File("icons/ChessAlpha2.ttf"));
        } catch (FontFormatException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    protected void fireActionPerformed(ActionEvent actionEvent)
    {
        if(Chess.clickBuffer == null)
        {
            //System.out.println("1");
            if(currentPiece != null && currentPiece.home == Chess.turn)
            {
                //System.out.println("2");
                Chess.clickBuffer = this;
                Chess.clickBuffer.setBackground(selected);

                for(int index = 0; index < Board.size; index++)
                    for(int jindex = 0; jindex < Board.size; jindex++)
                        if(currentPiece.isLegal(new Point(index, jindex)))
                            Chess.board.squares[index][jindex].setBackground(selected);
            }
        }
        else
        {
            if(this == Chess.clickBuffer)
            {
                //System.out.println("6");
                Board.resetColor();
                Chess.clickBuffer = null;
            }
            else
            {
                //System.out.println("4");
                if(currentPiece == null || currentPiece.home != Chess.turn)
                {
                    //System.out.println("5");
                    if(Chess.clickBuffer.currentPiece.move(new Point(row, col)))
                    {
                        //System.out.println("6");
                        Board.resetColor();
                        Chess.clickBuffer = null;
                        Chess.turn = !Chess.turn;
                    }
                }
            }
        }
    }

    @Override
    protected void paintComponent(Graphics graphics)
    {
        super.paintComponent(graphics);

        //graphics.setFont(chessFont);

        if(currentPiece != null)
//            setIcon(new ImageIcon());
            setIcon(new ImageIcon("icons/" + currentPiece.toString() + ".png"));
        else
            setIcon(null);
    }

    public Square(int i, int j)
    {
        this.row = i;
        this.col = j;

        if(i % Board.size < 2 || (i + 2) % Board.size < 2)
        {
            if(i == 1 || Board.size - 1 - i == 1)
                currentPiece = new Pawn(i, j);
            else
            {
                if(Board.size - 1 - j == 0 || j == 0)
                    currentPiece = new Rook(i, j);
                else if(Board.size - 1 - j == 1 || j == 1)
                    currentPiece = new Knight(i, j);
                else if(Board.size - 1 - j == 2 || j == 2)
                    currentPiece = new Bishop(i, j);
                else if(j == 3)
                    currentPiece = new Queen(i, j);
                else
                    currentPiece = new King(i, j);
            }
        }
    }

    public void check()
    {
        setBackground(Color.red);
        System.out.println("Check.");
    }
}
