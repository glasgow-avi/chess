package game;

import javax.swing.*;
import java.awt.*;

public class Board extends JFrame
{
    public static int size = 8;
    public static Square[][] squares = new Square[size][size];

    public static void resetColor()
    {
        for(int index = 0; index < Board.size; index++)
            for(int jindex = 0; jindex < Board.size; jindex++)
                squares[index][jindex].setBackground((index + jindex) % 2 == 0 ? Color.WHITE : Color.BLUE   );
    }

    public Board()
    {
        super("Chess");

        setSize(size / 2 * 100, size / 2 * 100);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new GridLayout(size, size));

        for(int index = 0; index < squares.length; index++)
            for(int jindex = 0; jindex < squares.length; jindex++)
            {
                squares[index][jindex] = new Square(index, jindex);
                this.add(squares[index][jindex]);
            }

        setResizable(false);

        resetColor();
        setVisible(true);
    }

    public static void printBoard()
    {
        for(int index = 0; index < squares.length; index++)
        {
            for(int jindex = 0; jindex < squares.length; jindex++)
                System.out.print(squares[index][jindex].currentPiece + "\t\t");
            System.out.println();
        }
    }
}
