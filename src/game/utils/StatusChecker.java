package game.utils;

import game.Board;
import game.Square;
import pieces.King;
import pieces.Piece;

import java.awt.*;

public class StatusChecker
{
    public static Piece check(boolean home)
    {
        Piece king = null;

        for(int index = 0; index < Board.size; index++)
            for(int jindex = 0; jindex < Board.size; jindex++)
                if(Board.squares[index][jindex].currentPiece != null && Board.squares[index][jindex].currentPiece instanceof King && Board.squares[index][jindex].currentPiece.home == home)
                    king = Board.squares[index][jindex].currentPiece;

        for(Square[] row : Board.squares)
            for(Square square : row)
                if(square.currentPiece != null && square.currentPiece.home != home)
                    if(square.currentPiece.isLegal(king.position))
                        return king;

        return null;
    }

    public static boolean checkmate(boolean home)
    {
        if(check(home) == null)
            return false;

        for(int index = 0; index < Board.size; index++)
        {
            for(int jindex = 0; jindex < Board.size; jindex++)
            {
                if(Board.squares[index][jindex].currentPiece != null && Board.squares[index][jindex].currentPiece.home == home)
                {
                    for(int c = 0; c < Board.size; c++)
                    {
                        for(int d = 0; d < Board.size; d++)
                        {
                            if(Board.squares[index][jindex].currentPiece.isLegal(new Point(c, d)) && !mockCheck(Board.squares[index][jindex].currentPiece, new Point(c, d), home))
                                return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public static boolean mockCheck(Piece piece, Point position, boolean home)
    {
        Piece oldPiece = Board.squares[position.x][position.y].currentPiece;
        Point oldPosition = piece.position;
        Board.squares[position.x][position.y].currentPiece = null;
        Board.squares[position.x][position.y].currentPiece = Board.squares[piece.position.x][piece.position.y].currentPiece;
        Board.squares[piece.position.x][piece.position.y].currentPiece = null;
        Board.squares[position.x][position.y].currentPiece.position = position;
        boolean legal = StatusChecker.check(home) == null;
        Board.squares[oldPosition.x][oldPosition.y].currentPiece = piece;
        Board.squares[position.x][position.y].currentPiece = oldPiece;
        Board.squares[oldPosition.x][oldPosition.y].currentPiece.position = oldPosition;
        return legal;
    }
}
