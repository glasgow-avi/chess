package pieces;

import game.Board;
import game.Chess;
import game.Square;
import static game.utils.StatusChecker.*;

import java.awt.*;

public abstract class Piece
{
    public boolean home;
    public Point position;

    private boolean dead = false;

    public Piece(int i, int j)
    {
        position = new Point(i, j);

        this.home = i > Board.size / 2;
    }

    public boolean isLegal(Point position)
    {
        if(position.x < 0 || position.y < 0 || position.y >= Board.size || position.x >= Board.size)
            return false;
        if(Board.squares[position.x][position.y].currentPiece != null && this.home == Board.squares[position.x][position.y].currentPiece.home)
            return false;

        if(Chess.turn == home)
            return mockCheck(this, position, home);
        return true;
    }

    public boolean move(Point position)
    {
        if(!isLegal(position))
            return false;

        Square destinationSquare = Board.squares[position.x][position.y];
        if(destinationSquare.currentPiece != null)
            destinationSquare.currentPiece.kill();

        Board.squares[position.x][position.y].currentPiece = this;
        Board.squares[this.position.x][this.position.y].currentPiece = null;
        System.out.println(this.position.x + " " + this.position.y + " " + position.x + " " + position.y);

        this.position = position;

//        Board.printBoard();

        Piece king = check(!home);
        if(king != null)
        {
            Board.squares[king.position.x][king.position.y].check();
        }

        if(checkmate(!home))
            System.out.println("Checkmate.");

        Chess.board.repaint();
        return true;
    }

    public void kill()
    {
        dead = true;
    }

    public boolean isDead()
    {
        return dead;
    }

    protected boolean testHorizontal(Point position)
    {
        int deltaX = (int) Math.signum(position.x - this.position.x), deltaY = (int) Math.signum(position.y - this.position.y);
        if(deltaX != 0 && deltaY != 0)
            return false;

        for(int x = this.position.x + deltaX, y = this.position.y + deltaY; y != position.y || x != position.x; x += deltaX, y+= deltaY)
        {
            if (Chess.board.squares[x][y].currentPiece != null)
                return false;
        }

        return Chess.board.squares[position.x][position.y].currentPiece == null || Chess.board.squares[position.x][position.y].currentPiece.home != this.home;
    }

    protected boolean testDiagonal(Point position)
    {
        int deltaX = position.x - this.position.x, deltaY = position.y - this.position.y;
        if(Math.abs(deltaY) != Math.abs(deltaX))
            return false;

        deltaX = (int) Math.signum(deltaX);
        deltaY = (int) Math.signum(deltaY);
        if(deltaY == 0 || deltaX == 0)
            return false;

        for(int x = this.position.x + deltaX, y = this.position.y + deltaY; x != position.x; x += deltaX, y+= deltaY)
        {
            if (Chess.board.squares[x][y].currentPiece != null)
                return false;
        }

        return Chess.board.squares[position.x][position.y].currentPiece == null || Chess.board.squares[position.x][position.y].currentPiece.home != this.home;
    }

    @Override
    public String toString()
    {
        return getClass().getTypeName().split("\\.")[getClass().getTypeName().split("\\.").length - 1] + " "
                + (home ? "(w)" : "(b)");
    }
}
