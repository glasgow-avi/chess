package pieces;

import java.awt.*;

public class Bishop extends Piece
{
    @Override
    public boolean isLegal(Point position)
    {
        return testDiagonal(position) && super.isLegal(position);
    }

    public Bishop(int i, int j)
    {
        super(i, j);
    }
}