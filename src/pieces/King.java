package pieces;

import java.awt.*;

public class King extends Piece
{
    @Override
    public boolean isLegal(Point position)
    {
        int deltaX = Math.abs(position.x - this.position.x), deltaY = Math.abs(position.y - this.position.y);
        boolean isLegal = deltaY < 2 && deltaX < 2;
        return isLegal && super.isLegal(position);
    }

    public King(int i, int j)
    {
        super(i, j);
    }
}
