package pieces;

import game.Chess;

import java.awt.*;

public class Pawn extends Piece
{
    public Pawn(int i, int j)
    {
        super(i, j);
    }

    @Override
    public boolean isLegal(Point position)
    {
        boolean superLegal = super.isLegal(position),
                oneStepForward = this.position.x - position.x == 1 && home || position.x - this.position.x == 1 && !home,
                nextSquareIsEmpty = Chess.board.squares[position.x][position.y].currentPiece == null,
                sameLine = position.y == this.position.y,
                killingOppositeSexPiece = Chess.board.squares[position.x][position.y].currentPiece != null && this.home != Chess.board.squares[position.x][position.y].currentPiece.home,
                oneStepSideWays = Math.abs(this.position.y - position.y) == 1;

        //System.out.println(superLegal + " " + oneStepForward + " " + nextSquareIsEmpty + " " + sameLine + " " + killingOppositeSexPiece + " " + oneStepSideWays);

        return superLegal &&
                sameLine && oneStepForward && nextSquareIsEmpty ||
                oneStepForward && killingOppositeSexPiece && oneStepSideWays;
    }
}