package pieces;

import java.awt.*;

public class Queen extends Piece
{
    @Override
    public boolean isLegal(Point position)
    {
        boolean isLegal = testDiagonal(position) || testHorizontal(position);
        return isLegal && super.isLegal(position);
    }

    public Queen(int i, int j)
    {
        super(i, j);
    }
}