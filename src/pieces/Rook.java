package pieces;

import java.awt.*;

public class Rook extends Piece
{
    @Override
    public boolean isLegal(Point position)
    {
        return testHorizontal(position) && super.isLegal(position);
    }

    public Rook(int i, int j)
    {
        super(i, j);
    }
}