package pieces;

import java.awt.*;

public class Knight extends Piece
{
    @Override
    public boolean isLegal(Point position)
    {
        int deltaY = Math.abs(position.y - this.position.y), deltaX = Math.abs(position.x - this.position.x);
        boolean isLegal = deltaY == 2 && deltaX == 1 || deltaY == 1 && deltaX == 2;

        return isLegal && super.isLegal(position);
    }

    public Knight(int i, int j)
    {
        super(i, j);
    }
}